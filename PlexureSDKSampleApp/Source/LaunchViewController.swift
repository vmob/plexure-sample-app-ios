//
//  LaunchViewController.swift
//  PlexureSDKSampleApp
//
//  Created by Ludy Su on 2/08/18.
//  Copyright © 2018 VMob Ltd. All rights reserved.
//

class LaunchViewController: UIViewController {

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let shouldSkip = UserDefaults.standard.bool(forKey: USERDEFAULTS_KEY_LOGIN_SKIPPED)
        if (shouldSkip || VMob.sharedInstance().authenticationManager().isLoggedIn()) {
            startMainScreen()
        } else {
            startAuthScreen()
        }
    }

}
