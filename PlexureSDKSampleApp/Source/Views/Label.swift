//
//  Label.swift
//  PlexureSDKSampleApp
//
//  Created by Kush Sharma on 13/08/15.
//  Copyright (c) 2015 Plexure. All rights reserved.
//

import UIKit

class Label: UILabel {
    override var bounds: CGRect{
        didSet{
            //your code here
            if numberOfLines == 0 && bounds.size.width != preferredMaxLayoutWidth {
                preferredMaxLayoutWidth = self.bounds.size.width
                setNeedsUpdateConstraints()
            }
        }
    }
}
