//
//  AdvertCell.swift
//  PlexureSDKSampleApp
//
//  Created by Kush Sharma on 12/08/15.
//  Copyright (c) 2015 Plexure. All rights reserved.
//

import UIKit

class AdvertCell: UITableViewCell {
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var titleLabel: Label!
    @IBOutlet weak var subtitleLabel: Label!
    @IBOutlet weak var customImageView: UIImageView!

    override func awakeFromNib() {
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
