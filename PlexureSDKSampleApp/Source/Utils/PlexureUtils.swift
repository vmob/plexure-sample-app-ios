//
//  PlexureUtils.swift
//  PlexureSDKSampleApp
//
//  Created by Kush Sharma on 17/08/15.
//  Copyright (c) 2015 Plexure. All rights reserved.
//

import Foundation

class PlexureUtils: NSObject {

    /**
     * The shared PlexureUtils object for the system.
     */
    static let sharedInstance = PlexureUtils()

    /**
     * Evaluate the current state of an offer accorsing to various fields
     * See the working with Offers Chapter at https://support.plexure.com/hc/en-us/categories/201194196-SDK Developer portal
     */
    func evaluateVoucherState(_ offer: VMOffer) -> String {
        //Is the offer available right now?
        if offer.isActive.intValue == 1 {
            //Yes, it can be redeemed
            return AVAILABLE
        } else {
            //Is this a respawning offer?
            if offer.isRespawning.intValue == 1 {
                //Yes, are there any days left before it respawns?
                if CInt(offer.respawnDaysNumber) == 0 {
                    //No, then it will be available today, just not yet
                    return NOT_AVAILABLE_RESPAWNABLE
                } else {
                    //Yes, it was redeemed already and still a few days to go until it will be available again
                    return REDEEMED_RESPAWNABLE
                }
            }
            
            //This is not a respawning offer
            //Was it redeemed previously?
            if CInt(offer.redemptionCount) == 0 {
                //No, it will be available, just not yet
                return NOT_AVAILABLE_NON_RESPAWNABLE
            }
            
            //Offer was redeemed previously and it is not respawnable: not available any more
            return REDEEMED_NON_RESPAWNABLE
        }
    }

}
