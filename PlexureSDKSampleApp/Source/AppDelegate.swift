//
//  AppDelegate.swift
//  PlexureSDKSampleApp
//
//  Created by Kush Sharma on 12/08/15.
//  Copyright (c) 2015 Plexure. All rights reserved.
//

import Crashlytics
import Fabric
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    var deviceToken: String = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // Override point for customization after application launch.
        if UIApplication.shared.responds(to:Selector("registerForRemoteNotifications")) {
            UIApplication.shared.registerForRemoteNotifications()
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: ([.sound, .alert, .badge]), categories: nil))
        }
        else {
            UIApplication.shared.registerForRemoteNotifications(matching: [.alert, .badge, .sound])
        }

        /**
             * The Plexure platform allows for tags to be added to consumers from mobile apps
             * which can be used for targeting content and push messages, and for use in analytics.
             */
        // tag as iphone and sample app on app launch complete
        //    [[[VMob sharedInstance] consumerManager] addTagValueReferenceCodes:@[@"iphone_miseru",@"sample_app"] completionBlock:nil];
        UITabBar.appearance().tintColor = UIColor(red: CGFloat(237.0 / 255.0), green: CGFloat(138.0 / 255.0), blue: CGFloat(45.0 / 255.0), alpha: CGFloat(1.0))
        Fabric.with([Crashlytics.self])
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

// MARK: - PushNotification Methods
    
    // We are registered, so now store the device token (as a string) on the AppDelegate instance
    // taking care to remove the angle brackets first.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let angleBrackets = CharacterSet.init(charactersIn:"<>")
        self.deviceToken = deviceToken.description.trimmingCharacters(in: angleBrackets)
        print("PushToken - \(self.deviceToken)")
    }

    // Handle any failure to register. In this case we set the deviceToken to an empty
    // string to prevent the insert from failing.
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error?) {
        print("Failed to register for remote notifications: \(error)")
        deviceToken = ""
    }

    // Because toast alerts don't work when the app is running, the app handles them.
    // This uses the userInfo in the payload to display a UIAlertView.
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("\(userInfo)")
        let apsData: [AnyHashable: Any]? = (userInfo["aps"] as? [AnyHashable: Any])
        let alert = UIAlertView(title: "iOS 6- Push Notification", message: (apsData?.description)!, delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
        alert.show()
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (_: UIBackgroundFetchResult) -> Void) {
        print("\(userInfo)")
        let apsData: [AnyHashable: Any]? = (userInfo["aps"] as? [AnyHashable: Any])
        let alert = UIAlertView(title: "iOS 7+ Push Notification", message: (apsData?.description)!, delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
        alert.show()
        completionHandler(.newData)
    }
}
