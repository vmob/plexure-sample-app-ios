//
//  UIViewControllerExtension.swift
//  PlexureSDKSampleApp
//
//  Created by Ludy Su on 2/08/18.
//  Copyright © 2018 VMob Ltd. All rights reserved.
//

import UIKit
import MBProgressHUD


extension UIViewController {


    func startMainScreen() {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainVC") as! UITabBarController
        present(controller, animated: true, completion: nil)
    }

    func startAuthScreen() {
        let controller = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "AuthVC") as! UIViewController
        present(controller, animated: false, completion: nil)
    }

    func showAlert(forTitle title: String, andMessage message: Any) {
        UIAlertView(title: title, message: "\(message)", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "").show()
    }

    /**
     * Show the progress Indicator
     */
    func showProgressHUD() {
        MBProgressHUD.showAdded(to: view, animated: true)
        MBProgressHUD(for: view).labelText = "Loading"
    }

    /**
     * Hide the progress Indicator
     */
    func hideProgressHUD() {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
    }

}
