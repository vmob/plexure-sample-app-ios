//
//  Constants.swift
//  PlexureSDKSampleApp
//
//  Created by Kush Sharma on 13/08/15.
//  Copyright (c) 2015 Plexure. All rights reserved.
//

import Foundation


// MARK: -  PAGE impressions

/**
 * Constants for Plexure SDK Activity tracking: names of various events and categories.
 * <p/>
 * This file contains a few examples for IDs which are sent by the app to the Plexure backend
 * as Plexure Activities.
 * Collected activities are vary project by project, in this file you can find examples for
 * the following types:
 * <ul>
 * <li>Page impressions (page IDs and categories)</li>
 * <li>Custom button click activities</li>
 * </ul>
 */

/**
 * Advertisements screen page impression
 */
let PAGE_IMPRESSION_ADVERTISEMENTS: String = "advertisements_screen"

/**
 * Offers screen page impression
 */
let PAGE_IMPRESSION_OFFERS: String = "offers_screen"

/**
 * Offer details screen page impression
 */
let PAGE_IMPRESSION_OFFER_DETAILS: String = "offers_details_screen"

/**
 * Venues screen page impression
 */
let PAGE_IMPRESSION_VENUES: String = "venues_screen"

/**
 * User screen page impression
 */
let PAGE_IMPRESSION_USER: String = "user_screen"

/** PAGE impression codes **/

let PAGE_IMPRESSION_CODE_CONTENT: String = "content"
let PAGE_IMPRESSION_CODE_MENU: String = "menu"

/** Button clicks **/

let BUTTON_CLICK_LOGIN: String = "login"
let BUTTON_CLICK_SIGNUP: String = "signup"
let BUTTON_CLICK_VENUE: String = "venue"
let BUTTON_CLICK_GET_TERMS: String = "terms_and_conditions"

/** Button clicks codes **/

let BUTTON_CLICK_CODE_GENERIC: String = "generic"


// MARK: - Offer States

/**
 * Offer is available for redemption
 */
let AVAILABLE: String = "available"

/**
 * Offer is not respawnable and not available currently, but it will be later on. The offer was not redeemed before.
 */
let NOT_AVAILABLE_NON_RESPAWNABLE: String = "available_non_resp"

/**
 * Offer is respawnable and not available currently, but will be available today.
 */
let NOT_AVAILABLE_RESPAWNABLE: String = "available_resp"

/**
 * Offer is not respawnable and it was redeemed previously: not available any longer.
 */
let REDEEMED_NON_RESPAWNABLE: String = "redeemed_non_resp"

/**
 * Offer is respawnable, but it was redeemed previously and a couple more days to go before it will be active again.
 */
let REDEEMED_RESPAWNABLE: String = "redeemed_resp"


// MARK: - UserDefaults keys

let USERDEFAULTS_KEY_LOGIN_SKIPPED = "login_skipped"
