//
//  VenuesViewController.swift
//  PlexureSDKSampleApp
//
// View Controller for fetching and displaying Venues from Plexure backend.
//
// This view controller displays the location of venues which are fetched from the Plexure backend.
// Together with the  MKMapKit, it implements a simple way for handling the process for getting
//  the venues and manage failure scenario.
//
//  Created by Kush Sharma on 14/08/15.
//  Copyright (c) 2015 Plexure. All rights reserved.
//

import MapKit
import UIKit

class VenuesViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    var venues : NSMutableArray = []
    var mapTable: NSMapTable<AnyObject, AnyObject>?

    override func viewDidLoad() {
        super.viewDidLoad()
        getVenues()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Send venues page impression Plexure Activity
        VMActivityTracker.sharedInstance().logPageImpression(withId: PAGE_IMPRESSION_VENUES, merchantId: nil, venueId: nil, itemId: nil, itemCode: PAGE_IMPRESSION_CODE_CONTENT)
    }

// MARK: - Venues Handling Methods
    func getVenues() {
        showProgressHUD()
            /*
                 * We supply 0 offset for the venues, but offset arguement in the
                 * following call would specify any required offset
                 *
                 * Call the SDK's method which will make the request for us
                 * Completion block handling is inlined for simplifying the code
                 */
        let criteria = VMVenueSearchCriteria()
        criteria.limit = Int(VENUE_LIMIT) as NSNumber
        criteria.offset = Int(VENUE_OFFSET) as NSNumber
        VMVenuesManager.sharedInstance().getVenuesWith(criteria, completionBlock: {(_ data: Any, _ error: Error?) -> Void in
            self.hideProgressHUD()
            if error == nil {
                self.venues.removeAllObjects();
                //Fresh list of advertisements is available, set it on the TableView
                if let vns = data as? NSArray {
                    for v in vns {
                        if let ad = v as? VMVenue {
                            self.venues.add(v)
                        }
                    }
                }

                
                if self.venues.count > 0 {
                    self.plotVenuesOnMap()
                }
                else {
                    self.showAlert(forTitle: "Venues not found!", andMessage: "No venues found at the moment.")
                }
            }
            else {
                self.showAlert(forTitle: "Error", andMessage: error)
            }
        })
    }

    func plotVenuesOnMap() {
        //Add all venues as markers to the map, also maintain the mapping between the markers and
        //the venues to be able to get data from the venue when the user clicks on the marker.
        mapTable = NSMapTable.strongToStrongObjects()
        let zoomLocation = CLLocationCoordinate2D(latitude: CDouble((venues[0] as! VMVenue).latitude), longitude: CDouble((venues[0] as! VMVenue).longitude))
        let viewRegion: MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, CLLocationDistance(150 * METERS_PER_KM), CLLocationDistance(150 * METERS_PER_KM))
        mapView.setRegion(viewRegion, animated: true)
        for data: Any in self.venues {
            let venue: VMVenue? = data as! VMVenue
            if (venue != nil && venue?.latitude != nil && venue?.longitude != nil) {
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2DMake(CDouble((venue?.latitude)!), CDouble((venue?.longitude)!))
                annotation.title = venue?.name
                annotation.subtitle = venue?.address
                mapView.addAnnotation(annotation)
                mapTable?.setObject(venue, forKey: annotation)
            }
        }
    }

// MARK: - MapView Delegate Methods
    func mapView(_ mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKPinAnnotationView? {
        // Handle any custom annotations.
        if (annotation is MKPointAnnotation) {
                // Try to dequeue an existing pin view first.
            let venuePin: String = "VenueAnnotationView"
            var pinView: MKPinAnnotationView? = (mapView.dequeueReusableAnnotationView(withIdentifier: venuePin) as? MKPinAnnotationView)
            if pinView == nil {
                // If an existing pin view was not available, create one.
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: venuePin)
                pinView?.animatesDrop = true
                pinView?.canShowCallout = true
                pinView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            else {
                pinView?.annotation = annotation
            }
            return pinView!
        }
        return nil
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
            //Try to find the venue by the clicked annotation in the map table
        let venue: VMVenue? = mapTable?.object(forKey: view.annotation) as! VMVenue
        /*
            * Send Plexure Activity for the venue click.
            * We assume a generic click in this case, but specific information can be served as
            * context for the click via parameters which are not specified here.
            * The button click ID and the venue ID together gives enough information about the event.
            */
        VMActivityTracker.sharedInstance().logButtonClick(withId: BUTTON_CLICK_VENUE, merchantId: nil, venueId: venue?.contentId, itemId: nil, itemCode: BUTTON_CLICK_CODE_GENERIC)
            /*
                 * Give travel direction to the user to the selected venue
                 */
        let place = MKPlacemark(coordinate: (view.annotation?.coordinate)!, addressDictionary: nil)
        let destination = MKMapItem(placemark: place)
        destination.name = (view.annotation?.title)!
        let items: [Any] = [destination]
        let options: [AnyHashable: Any] = [
                MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving
            ]

        MKMapItem.openMaps(with: items as! [MKMapItem], launchOptions: options as! [String : Any])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

let METERS_PER_KM = 1000
let VENUE_OFFSET = 0
let VENUE_LIMIT = 50
