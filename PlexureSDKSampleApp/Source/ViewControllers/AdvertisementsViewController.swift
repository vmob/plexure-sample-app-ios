//
//  AdvertisementsViewController.swift
//  PlexureSDKSampleApp
//
// View Controller for fetching and displaying advertisements from Plexure backend.
//
// This view controller displays the list of advertisements which are fetched from the Plexure backend.
// Together with the  @AdvertCell, it implements a simple way for handling the process for getting
//  the advertisements and manage failure scenario.
//
//  Created by Kush Sharma on 12/08/15.
//  Copyright (c) 2015 Plexure. All rights reserved.
//

import SDWebImage
import UIKit

private let AdvertCellIdentifier: String = "AdvertCell"

class AdvertisementsViewController: UIViewController {
    @IBOutlet weak var tableViewOutlet: UITableView!

    var advertisments : NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAdvertisements()
        //Send advertisement page impression Plexure Activity
        VMActivityTracker.sharedInstance().logPageImpression(withId: PAGE_IMPRESSION_ADVERTISEMENTS, merchantId: nil, venueId: nil, itemId: nil, itemCode: PAGE_IMPRESSION_CODE_CONTENT)
    }

// MARK: - Advertisements Handling Methods

    func getAdvertisements() {
        showProgressHUD()
        /*
         * We don't set up any filtering for the advertisements, but arguments in the
         * following call would specify any required filtering
         *
         * Call the SDK's method which will make the request for us
         * Completion block handling is inlined for simplifying the code
         */
        VMAdvertisementsManager.sharedInstance().getAdvertisementsWith(nil, completionBlock: {(advertisementList, error) in
            self.hideProgressHUD()
            if error == nil {
                self.advertisments.removeAllObjects();
                //Fresh list of advertisements is available, set it on the TableView
                if let ad = advertisementList as? NSArray {
                    for v in ad {
                        if let ad = v as? VMAdvertisement {
                            // only if active
                            if ad.isActive.boolValue {
                                self.advertisments.add(ad)
                            }
                        }
                    }
                }

                self.reloadTableViewContent()
            }
            else {
                //Something went wrong, let's show the error on the screen
                self.showAlert(forTitle: "Error", andMessage: error)
            }
        })
    }

// MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return advertisments.count
    }

    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell: AdvertCell? = tableView.dequeueReusableCell(withIdentifier: AdvertCellIdentifier, for: indexPath as IndexPath) as! AdvertCell
        cell?.container?.layer.cornerRadius = 7.0
        cell?.container?.layer.masksToBounds = true
        cell?.container?.layer.borderColor = UIColor.lightGray.cgColor
        cell?.container?.layer.borderWidth = 0.5
        configureCell(cell!, at: indexPath as IndexPath)
        return cell!
    }

    func configureCell(_ cell: AdvertCell, at indexPath: IndexPath) {
        let item: VMAdvertisement? = advertisments[indexPath.row] as! VMAdvertisement
        setTitleFor(cell, item: item!)
        setSubtitleFor(cell, item: item!)
        setImageFor(cell, item: item!)
        /**
         * Send advertisement impression Plexure Activity.
         * This Activity must be sent for ads which have been actually displayed (and only for those).
         * In a standard list view the method can be called when the view for the list item is
         * populated (getView method, like here).
         *  This simple solution might not work for other screen layout designs or components, most
         *  notably it is not working with Reused view. If that is the case, the displayed list items must be
         *  tracked separately and the method must be called when an advertisement was displayed for
         *  the first time or displayed again after it disappeared from the screen.
         **/
        VMActivityTracker.sharedInstance().logAdImpression(withId: CInt((item?.contentId)!), channel: nil, placement: item?.placement)
    }

    func setTitleFor(_ cell: AdvertCell, item: VMAdvertisement) {
        cell.titleLabel?.text = item.title
    }

    func setSubtitleFor(_ cell: AdvertCell, item: VMAdvertisement) {
        cell.subtitleLabel.text = item.contentDescription
    }

    func setImageFor(_ cell: AdvertCell, item: VMAdvertisement) {
        let imgURLString : NSString? = item.getImageUrl(withWidth: nil, height: nil, imageFormat: "PNG") as? NSString
        if (imgURLString != nil) {
            let url = URL(string: (imgURLString as? String)!)
            cell.customImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        } else {
            cell.customImageView.sd_setImage(with: nil, placeholderImage: UIImage(named: "placeholder"))
        }
    }

// MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item: VMAdvertisement? = advertisments[indexPath.row] as! VMAdvertisement
        /*
             * Handling row tap: opening URL from advertisement as a generic URL.
             * It is up to the implementation project how to handle the click through URLs from
             * an advertisement.
             */
        if (item?.clickThroughUrl?.characters.count)! > 0 {
            UIApplication.shared.openURL(URL(string: (item?.clickThroughUrl)!)!)
        }
        else {
            showAlert(forTitle: "Weird!", andMessage: "No clickthrough URL found.")
        }
        //Send advertisement click Plexure Activity
        VMActivityTracker.sharedInstance().logAdImpression(withId: CInt((item?.contentId)!), channel: nil, placement: item?.placement)
        self.tableViewOutlet.deselectRow(at: self.tableViewOutlet.indexPathForSelectedRow!, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForCell(at: indexPath)
    }

    func heightForCell(at indexPath: IndexPath) -> CGFloat {
        var sizingCell = self.tableViewOutlet.dequeueReusableCell(withIdentifier:AdvertCellIdentifier) as! AdvertCell
        configureCell(sizingCell, at: indexPath)
        return calculateHeight(forConfiguredSizingCell: sizingCell)
    }

    func calculateHeight(forConfiguredSizingCell sizingCell: UITableViewCell) -> CGFloat {
        sizingCell.bounds = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(self.tableViewOutlet.frame.width), height: CGFloat(sizingCell.bounds.height))
        sizingCell.setNeedsLayout()
        sizingCell.layoutIfNeeded()
        let size: CGSize = sizingCell.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        return size.height + 1.0
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220.0
    }

// MARK: - Misc. Methods

    func reloadTableViewContent() {
        DispatchQueue.main.async(execute: {() -> Void in
            self.tableViewOutlet.reloadData()
            self.tableViewOutlet.scrollRectToVisible(CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(1), height: CGFloat(1)), animated: false)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
