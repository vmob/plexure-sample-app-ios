//
//  AuthenticationViewController.swift
//  PlexureSDKSampleApp
//
//  Created by Ludy Su on 2/08/18.
//  Copyright © 2018 VMob Ltd. All rights reserved.
//

class AuthenticationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        //Send User page impression Plexure Activity if SDK is initialised
        if VMob.sharedInstance().initStatus() == VMSDKInitStatus_Success {
            VMActivityTracker.sharedInstance().logPageImpression(withId: PAGE_IMPRESSION_USER, merchantId: nil, venueId: nil, itemId: nil, itemCode: PAGE_IMPRESSION_CODE_CONTENT)
        }
    }

// MARK: - Button Tap Handlers

    /**
     * Prompt user to input username and password.
     */
    @IBAction func doLoginAction(_ sender: Any) {
        createAlertController(type: .login)
    }

    /**
     * Prompt user to input email address and password.
     */
    @IBAction func doSignUpAction(_ sender: Any) {
        createAlertController(type: .signup)
    }

    @IBAction func doForgotPasswordAction(_ sender: Any) {
        createAlertController(type: .forgot)
    }

    @IBAction func doSkipAction(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: USERDEFAULTS_KEY_LOGIN_SKIPPED)
        startMainScreen()
    }

    private func createAlertController(type: AlertControllerType) {
        let title: String
        switch type {
        case .login:
            title = "Login"
        case .signup:
            title = "Sign up"
        case .forgot:
            title = "Forgot password"
        }
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)

        alertController.addTextField() {(textField: UITextField) -> Void in
            textField.placeholder = NSLocalizedString("Email address", comment: "Email address")
            textField.addTarget(self, action: #selector(self.alertTextFieldDidChange), for: .editingChanged)
            textField.keyboardType = .emailAddress
        }

        if (type != .forgot) {
            alertController.addTextField() {(_ textField: UITextField) -> Void in
                textField.placeholder = NSLocalizedString("Password", comment: "Password")
                textField.isSecureTextEntry = true
                textField.addTarget(self, action: #selector(self.alertTextFieldDidChange), for: .editingChanged)
            }
        }

        present(alertController, animated: true, completion: nil)

        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .default) {(action: UIAlertAction) -> Void in
            self.showProgressHUD()

            let completionBlock: VMobCompletionBlock = {(data: Any, error: Error?) -> Void in
                self.hideProgressHUD()

                if error != nil {
                    if UInt32((error as! NSError).code) == VMHTTPCodeConflict.rawValue {
                        self.showAlert(forTitle: "User already exists", andMessage: "Try to use another email address")
                    } else {
                        self.showAlert(forTitle: "\(title) error:", andMessage: "error: \(error)")
                    }
                } else {
                    self.startMainScreen()
                }
            }

            switch type {
            case .login:
                let loginInfo = VMLoginInfo()
                loginInfo.username = alertController.textFields![0].text
                loginInfo.password = alertController.textFields![1].text

                VMAuthenticationManager.sharedInstance().login(with: loginInfo, completionBlock: completionBlock)
            case .signup:
                let signUpInfo = VMSignUpInfo()
                signUpInfo.signUpType = VMSignUpEmail
                signUpInfo.emailAddress = alertController.textFields![0].text
                signUpInfo.password = alertController.textFields![1].text

                VMAuthenticationManager.sharedInstance().signUp(with: signUpInfo, completionBlock: completionBlock)
            case .forgot:
                VMAuthenticationManager.sharedInstance().requestPasswordReset(withUsername: alertController.textFields![0].text, completionBlock: completionBlock)
            }
        }

        okAction.isEnabled = false
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel action"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            print("Action canceled")
        })

        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
    }
    
    /**
     * Controlling state of the action button based on the valid input
     */
    func alertTextFieldDidChange(_ sender: UITextField) {
        guard let alertController = (self.presentedViewController as? UIAlertController) else {
            return
        }

        let textFields = alertController.textFields
        let password = (textFields?.count)! >= 2 ? textFields?[1] : nil
        let okAction = alertController.actions[0]

        if (textFields?[0].text?.characters.count)! > 2 && (password == nil || (password!.text?.characters.count)! > 2) {
            okAction.isEnabled = true
        } else {
            okAction.isEnabled = false
        }
    }

// MARK: - enum

    private enum AlertControllerType {
        case login
        case signup
        case forgot
    }
}
