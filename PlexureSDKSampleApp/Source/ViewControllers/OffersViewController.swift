//
//  OffersViewController.swift
//  PlexureSDKSampleApp
//
// View Controller for fetching and displaying Offers from Plexure backend.
//
// This view controller displays the list of offers which are fetched from the Plexure backend.
// Together with the  @OfferCell, it implements a simple way for handling the process for getting
//  the offers and manage failure scenario.
//
//  Created by Kush Sharma on 12/08/15.
//  Copyright (c) 2015 Plexure. All rights reserved.
//

import SDWebImage
import UIKit

private let OfferCellIdentifier: String = "OfferCell"

class OffersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var offers : NSMutableArray = []
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getOffers()
        //Send offers page impression Plexure Activity
        VMActivityTracker.sharedInstance().logPageImpression(withId: PAGE_IMPRESSION_OFFERS, merchantId: nil, venueId: nil, itemId: nil, itemCode: PAGE_IMPRESSION_CODE_CONTENT)
    }

// MARK: - Offers Handling Methods
    func getOffers() {
        showProgressHUD()
        /*
             * We don't set up any filtering for the offers, but criteria arguement in the
             * following call would specify any required filtering
             *
             * Call the SDK's method which will make the request for us
             * Completion block handling is inlined for simplifying the code
             */
        VMOffersManager.sharedInstance().getOffersWith(nil, completionBlock: {(_ data: Any, _ error: Error?) -> Void in
            self.hideProgressHUD()
            if error == nil {
                self.offers.removeAllObjects();
                //Fresh list of advertisements is available, set it on the TableView
                if let offersData = data as? NSArray {
                    for offer in offersData {
                        if let of = offer as? VMOffer {
                            self.offers.add(of)
                        }
                    }
                }
                self.reloadTableViewContent()
            }
            else {
                self.showAlert(forTitle: "Error", andMessage: error)
            }
        })
    }

// MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OfferCell? = tableView.dequeueReusableCell(withIdentifier: OfferCellIdentifier, for: indexPath) as! OfferCell
        cell?.container?.layer.cornerRadius = 7.0
        cell?.container?.layer.masksToBounds = true
        cell?.container?.layer.borderColor = UIColor.lightGray.cgColor
        cell?.container?.layer.borderWidth = 0.5
        configureCell(cell!, at: indexPath)
        return cell!
    }

    func configureCell(_ cell: OfferCell, at indexPath: IndexPath) {
        let item: VMOffer? = offers[indexPath.row] as! VMOffer
        setTitleFor(cell, item: item!)
        setSubtitleFor(cell, item: item!)
        setStausFor(cell, item: item!)
        setImageFor((cell as? Any as! OfferCell), item: item!)
        /**
             * Send offer impression Plexure Activity.
             * This Activity must be sent for offers which have been actually displayed (and only for those).
             * In a standard list view the method can be called when the view for the list item is
             * populated (getView method, like here).
             *  This simple solution might not work for other screen layout designs or components, most
             *  notably it is not working with Reused view. If that is the case, the displayed list items must be
             *  tracked separately and the method must be called when an offer was displayed for
             *  the first time or displayed again after it disappeared from the screen.
             **/
        VMActivityTracker.sharedInstance().logOfferImpression(withId: CInt((item?.contentId)!), offerType: "", placementType: "")
    }

    func setTitleFor(_ cell: OfferCell, item: VMOffer) {
        cell.titleLabel?.text = item.title
    }

    func setStausFor(_ cell: OfferCell, item: VMOffer) {
        cell.statusLabel.text = "\((item.isActive != false) ? "Available" : "Not Available")"
    }

    func setSubtitleFor(_ cell: OfferCell, item: VMOffer) {
        cell.subtitleLabel.text = item.contentDescription
    }

    func setImageFor(_ cell: OfferCell, item: VMOffer) {
        let imgURLString : NSString? = item.getAlternativeImageUrl(withWidth: nil, height: nil, imageFormat: "PNG") as? NSString
        if (imgURLString != nil) {
            let url = URL(string: (imgURLString as? String)!)
            cell.customImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        } else {
            cell.customImageView.sd_setImage(with: nil, placeholderImage: UIImage(named: "placeholder"))
        }
    }

// MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item: VMOffer? = offers[indexPath.row] as! VMOffer
        //Send offer click Plexure Activity
        VMActivityTracker.sharedInstance().logOfferClick(withId: CInt((item?.contentId)!), offerType: "", placementType: "")
        tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForCell(at: indexPath)
    }

    func heightForCell(at indexPath: IndexPath) -> CGFloat {
        var sizingCell: OfferCell? = self.tableView.dequeueReusableCell(withIdentifier: OfferCellIdentifier) as! OfferCell
        
        configureCell(sizingCell!, at: indexPath)
        return calculateHeight(forConfiguredSizingCell: sizingCell!)
    }

    func calculateHeight(forConfiguredSizingCell sizingCell: UITableViewCell) -> CGFloat {
        sizingCell.bounds = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(self.tableView.frame.width), height: CGFloat(sizingCell.bounds.height))
        sizingCell.setNeedsLayout()
        sizingCell.layoutIfNeeded()
        let size: CGSize = sizingCell.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        return size.height + 1.0
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240.0
    }

// MARK: - Misc. Methods
    func reloadTableViewContent() {
        DispatchQueue.main.async(execute: {() -> Void in
            self.tableView.reloadData()
            self.tableView.scrollRectToVisible(CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(1), height: CGFloat(1)), animated: false)
        })
    }

// MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /*
         * Handling row tap: opening offer details in a new Screen.
         * It is up to the implementation project how to handle the details display for
         * an offer.
         */
        if (segue.identifier == "OfferDetails") {
            let selectedRowIndex: IndexPath? = self.tableView.indexPathForSelectedRow
            let detailViewController: OfferDetailsViewController? = segue.destination as! OfferDetailsViewController
            detailViewController?.offer = offers[(selectedRowIndex?.row)!] as! VMOffer
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
