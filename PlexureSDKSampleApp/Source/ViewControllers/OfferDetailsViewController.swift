//
//  OfferDetailsViewController.swift
//  PlexureSDKSampleApp
//
// View Controller for showing the Offer details for the selected offer
//
//  Created by Kush Sharma on 13/08/15.
//  Copyright (c) 2015 Plexure. All rights reserved.
//

import SDWebImage
import UIKit

class OfferDetailsViewController: UIViewController {
    var offer: VMOffer?

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var titleLabel: Label!
    @IBOutlet weak var subtitleLabel: Label!
    @IBOutlet weak var statusLabel: Label!
    @IBOutlet weak var startDateLabel: Label!
    @IBOutlet weak var endDateLabel: Label!
    @IBOutlet weak var redeemDateLabel: Label!
    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var redeemButton: UIButton!
    @IBOutlet weak var redemptionCodeLabel: UILabel!
    @IBOutlet weak var redemptionCodeTitleLabel: Label!

    var offerState: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Send offer details page impression Plexure Activity
        VMActivityTracker.sharedInstance().logPageImpression(withId: PAGE_IMPRESSION_OFFER_DETAILS, merchantId: nil, venueId: nil, itemId: nil, itemCode: PAGE_IMPRESSION_CODE_CONTENT)
    }

// MARK: - UI Handlers
    func updateUI() {
        container.layer.cornerRadius = 7.0
        container.layer.masksToBounds = true
        container.layer.borderColor = UIColor.lightGray.cgColor
        container.layer.borderWidth = 0.5
        titleLabel?.text = offer?.title
        subtitleLabel.text = offer?.contentDescription
        statusLabel.text = "\(((offer?.isActive) != nil) ? "Available" : "Not Available")"

        let imgURLString : NSString? = offer?.getImageUrl(withWidth: nil, height: nil, imageFormat: "PNG") as? NSString
        if (imgURLString != nil) {
            let url = URL(string: (imgURLString as? String)!)
            customImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        } else {
            customImageView.sd_setImage(with: nil, placeholderImage: UIImage(named: "placeholder"))
        }

        startDateLabel.text = "Start date: \(getStringFrom((offer?.startDate)!))"
        endDateLabel.text = "End date: \(getStringFrom((offer?.endDate)!))"
        offerState = PlexureUtils.sharedInstance.evaluateVoucherState(offer!)
        handleButtonState(withOfferState: offerState)
    }

    /**
     * Handle the visibility of the Redeem button based on the offer state
     */
    func handleButtonState(withOfferState offerState: String) {
        //Was this offer redeemed previously and not available currently?
        if (offerState == REDEEMED_NON_RESPAWNABLE) || (offerState == REDEEMED_RESPAWNABLE) {
            //Offer was redeemed previously and not available currently:
            //hide redeem button and get the redemption code from the platform again
            redeemButton.isHidden = true
            getRedemptionCode()
        }
        else {
            //Redeem button is enabled only if the offer can be redeemed (is active),
            //see offer state evaluation: PlexureUtils.evaluateVoucherState()
            redeemButton.isHidden = !(offerState == AVAILABLE)
        }
    }

    /**
     * This method is called when the offer has already been redeemed and we received a redemption code.
     * It changes the visual appearance of the fragment: redeem button will be hidden,
     * redemption code will be visible.
     */
    func handleUI(for redeemedOffer: VMRedeemedOffer) {
        //Hide redeem button, the offer was redeemed already
        redeemButton.isHidden = true
        //Show redemption code and date
        redemptionCodeTitleLabel.isHidden = false
        redemptionCodeLabel.isHidden = false
        redemptionCodeLabel.text = redeemedOffer.redemptionText
        redeemDateLabel.isHidden = false
        redeemDateLabel.text = "Redeemed at: \(getStringFrom(redeemedOffer.redemptionDate))"
    }

// MARK: - Button Tap Handlers
    /**
     * Get the terms and conditions text from the platform.
     * Please note: the text can be rather long, it is better to get it, only when necessary.
     */
    @IBAction func termsClickHandler(_ sender: Any) {
        showProgressHUD()
        VMOffersManager.sharedInstance().getTermsAndConditions(withOfferId: CInt((offer?.contentId)!), completionBlock: {(_ data: Any, _ error: Error?) -> Void in
            self.hideProgressHUD()
            if error == nil {
                self.showAlert(forTitle: "Offer terms", andMessage: "\(data)")
            }
            else {
                self.showAlert(forTitle: "Error", andMessage: error)
            }
        })
        VMActivityTracker.sharedInstance().logButtonClick(withId: BUTTON_CLICK_GET_TERMS, merchantId: nil, venueId: nil, itemId: offer?.contentId, itemCode: BUTTON_CLICK_CODE_GENERIC)
    }

    /**
     * Redeem the offer and handle the result
     */
    @IBAction func redeemOfferClickHandler(_ sender: Any) {
        showProgressHUD()
        VMOffersManager.sharedInstance().redeemOffer(withId: CInt((offer?.contentId)!), giftId: nil, offerInstanceId: nil, completionBlock: {(data, error)  in
            self.hideProgressHUD()
            let redeemedOffer : VMRedeemedOffer = data as! VMRedeemedOffer
            if error == nil {
                self.showAlert(forTitle: "Redemption successful!", andMessage: redeemedOffer.contentDescription)
                self.getRedemptionCode()
            }
            else {
                if UInt32((error as! NSError).code) == VMHTTPCodeConflict.rawValue {
                    self.showAlert(forTitle: "Error - Already redeemed.", andMessage: error)
                }
                else {
                    self.showAlert(forTitle: "Error - Unkown", andMessage: error)
                }
            }
        })
    }

// MARK: - Misc. Methods
    /**
     * When an offer has been redeemed previously, we need the redemption code whenever the user
     * comes back to the offer. This method is trying to get the redemption data from the platform
     * and if it was available then match it for the offer and show it.
     * Please note: it is possible that the redemption data was not available anymore, your app
     * must be able to handle that situation.
     */
    func getRedemptionCode() {
        showProgressHUD()
        let criteria = VMRedeemedOfferSearchCriteria()
        criteria.isIgnoreStartEndDates = true
        VMOffersManager.sharedInstance().getRedeemedOffers(with: criteria, completionBlock: {(_ data: Any, _ error: Error?) -> Void in
            self.hideProgressHUD()
            if error == nil {
                let redeemedOffers: [VMRedeemedOffer]? = (data as? [VMRedeemedOffer])
                for redeemedOffer: VMRedeemedOffer in redeemedOffers! {
                    if (redeemedOffer.offerId == self.offer?.contentId) {
                        self.handleUI(for: redeemedOffer)
                        return
                    }
                }
                self.showAlert(forTitle: "Error", andMessage: "Could not find previously redeemed offer")
            }
            else {
                self.showAlert(forTitle: "Error", andMessage: error)
            }
        })
    }

    func getStringFrom(_ date: Date) -> String {
        let dateString = DateFormatter.localizedString(from: date, dateStyle: .long, timeStyle: .short)
        return dateString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
