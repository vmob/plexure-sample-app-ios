//
//  ProfileViewController.swift
//  PlexureSDKSampleApp
//
//  Created by Ludy Su on 2/08/18.
//  Copyright © 2018 VMob Ltd. All rights reserved.
//

class ProfileViewController: UIViewController {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var actionButton: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        if VMob.sharedInstance().authenticationManager().isLoggedIn() == true {
            titleLable.isHidden = true
            actionButton.setTitle("LOGOUT", for: .normal)
        }
    }

    @IBAction func buttonAction(_ sender: Any) {
        if VMob.sharedInstance().authenticationManager().isLoggedIn() == true {
            let alert = UIAlertController(title: "Logout", message: "Do you want to logout?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: .default) { (_) in
                VMob.sharedInstance().authenticationManager().logout { (data, error) in
                    if error != nil {
                        self.showAlert(forTitle: "Error", andMessage: "Error while logging out")
                    } else {
                        self.startAuthScreen()
                    }
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        } else {
            startAuthScreen()
        }
    }


}
