# Sample app for Plexure iOS SDK

## Introduction
This source code implements a basic iOS app which makes use of various features of the Plexure SDK.

IMPORTANT: Plexure (formerly VMob Ltd.) was re-branded in 2016. In this app, the occurrences of the previous name (i.e., VMob)
were replaced by Plexure accordingly, where possible. In the SDK source code, the re-branding is scheduled for version 5. Thus,
the old name (i.e., VMob) is still being used in the current SDK version (4.x.x).

## Setup
The source code is written in Swift, can be built directly using the Xcode.

Before you begin using the source code you need to add a file which is not part of the source
code, but required for compiling and running the sample app:

### Plexure SDK configuration
To be able to use the Plexure SDK, you first need add additional *Info.plist* keys that grouped under a top level *VMobSDKConfiguration* key.

The possible configuration items are documented in the getting started section in the
[Plexure SDK documentation]. There are two required items, which must be present:

* *Site_ID* - Site ID for the VMob server
* *AuthorizationKey* - Authentication key for the VMob Server

An example for the configuration files structure:

```
<key>VMobSDKConfiguration</key>
     <dict>
        <key>AuthorizationKey</key>
        <string> ...Plexure server site ID... </string>
        <key>Site_ID</key>
        <string> ...Plexure server authentication key... </string>
    </dict>
```

## How to use the sources
After successful configuration the application can be built using standard build process.
The app can be installed and ran on any device which has iOS 9.0 or higher.

Since this source code implements a functioning application it contains more lines than what would
be absolutely necessary to make use of the Plexure SDK.
Please review the comments in the source files thoroughly, important details are described about
the implementation details for the Plexure SDK.

## See also
For further information please refer to the [Plexure SDK documentation].

[Plexure SDK documentation]:https://mobile.plexure.com/getting_started/introduction.html
